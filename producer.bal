import ballerinax/kafka;
import ballerina/io;

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

public function main() returns error? {
    string message = io:readln("Type message you would like to store: ");
    json m = {content: message};
    // Sends the message to the Kafka topic.
    check kafkaProducer->send({
                                topic: "kafkaTopic",
                                value: message.toBytes() });

    // Flushes the sent messages.
    check kafkaProducer->'flush();
}
